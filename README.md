# car_park

Car Park Node JS

## First Clone Repository
1. run "cp .env_local .env"
2. run "npm i"
## For Database
1. run "docker-compose up -d --build" > for create container Database
2. Open Browser go to http://localhost:5001/ root:root

## Run Project
1. run "npm start"

## Run Unit test
1. run "npm run test"

## Import postman for test api
1. import file postman from /postman/Car Park.postman_collection.json

## Spec api
1. Open Browser go to http://localhost:8000/api-doc/