const express = require('express');
const { body, param } = require('express-validator');
const SlotController = require(`../controllers/v1/SlotController`);
const router = express.Router();

// ----- Start Create Slot Car Park
const RuleCreate = [
    body('slotFloor', 'The floor is require. The length is between 1 to 2')
        .notEmpty()
        .isLength({ min: 1, max: 2 }),
    body('total', 'Total total is require. Must be a number between 1 to 100')
        .notEmpty()
        .isInt({
            min: 1,
            max: 100,
        }),
    body('carSize', 'Car Size ID is require. Must be a number between 1 to 3')
        .notEmpty()
        .isInt({
            min: 1,
            max: 3,
        }),
];

router.post('/lists', RuleCreate, SlotController.createSlots);
// ----- End Create Slot Car Park

// ----- Start Leave or Park The Car
const RuleParkCar = [
    body('slotID', 'ID is require must be a number').notEmpty().isInt(),
    body(
        'slotStatus',
        'The status number is require. The length is between 1 or 0'
    )
        .notEmpty()
        .isInt({
            min: 0,
            max: 1,
        }),
    body(
        'slotPlate',
        'The plate number is require. The length is between 1 to 4'
    )
        .if(body('slotStatus').custom((val) => val !== 1))
        .notEmpty(),
];
router.patch('/park', RuleParkCar, SlotController.park);
// ----- End Leave or Park The Car

// ----- Start Get Status per lot
const RuleStatusParkCar = [
    param('slotID', 'ID is require must be a number').notEmpty().isInt(),
];
router.get('/park/:slotID', RuleStatusParkCar, SlotController.get);
// ----- End Get Status per lot

// ----- Start Get Registration plate number
const RuleListsParkCar = [
    param('carSize', 'Car Size ID is require. Must be a number between 1 to 3')
        .notEmpty()
        .isInt({
            min: 1,
            max: 3,
        }),
    param('page', 'Page must be a number min 1').isInt({
        min: 1,
    }),
];
router.get(
    '/lists/plate/:carSize/:page',
    RuleListsParkCar,
    SlotController.getListsPlateNumber
);
// ----- End Get List Parking lot

// ----- Start Get Status Parking lot
router.get('/lists', SlotController.getLists);
// ----- End Get Status Parking lot

// ----- Start Allocated slot number
const RuleAllocated = [
    param('carSize', 'Car Size ID is require. Must be a number between 1 to 3')
        .notEmpty()
        .isInt({
            min: 1,
            max: 3,
        }),
];
router.get('/allocated/:carSize', RuleAllocated, SlotController.allocated);
// ----- End Get Allocated slot number

module.exports = router;
