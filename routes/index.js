const express = require('express');
const apiVersion = process.env.API_VERSION || 'v1';
const apiName = process.env.API_NAME || 'Carpark Service';

module.exports = (app) => {
    app.all(`/`, (req, res, next) => {
        res.status(200).json({
            code: 200,
            status: 'OK',
            message: `${apiName}`,
        });
    });

    app.use(
        express.urlencoded({
            extended: true,
        })
    );

    app.use(express.json());

    app.use(`/car_park/${apiVersion}/slots`, require(`./slot`));
};
