/**
 * @swagger
 * /car_park/v1/slots/allocated/{carSize}:
 *   get:
 *      description: Allocate parking by car size
 *      tags:
 *          - Car park
 *      parameters:
 *        - in: path
 *          name: carSize
 *          schema:
 *            type: integer
 *          required: true
 *      responses:
 *        200:
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#definitions/200'
 */
