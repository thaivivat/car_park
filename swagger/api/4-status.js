/**
 * @swagger
 * /car_park/v1/slots/lists:
 *   get:
 *      description: Get status parking lot
 *      tags:
 *          - Car park
 *      responses:
 *        200:
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#definitions/200'
 */
