/**
 * @swagger
 * /car_park/v1/slots/park:
 *   patch:
 *      description: Leave or Park the car
 *      tags:
 *          - Car park
 *      requestBody:
 *          content:
 *              'application/json':
 *                  schema:
 *                      $ref: '#definitions/ParkCar'
 *      responses:
 *        200:
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#definitions/200'
 *        400:
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#definitions/400Park'
 */
