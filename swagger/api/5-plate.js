/**
 * @swagger
 * /car_park/v1/slots/lists/plate/{carSize}/{page}:
 *   get:
 *      description: Get registration plate number list by car size
 *      tags:
 *          - Car park
 *      parameters:
 *        - in: path
 *          name: carSize
 *          schema:
 *            type: integer
 *          required: true
 *        - in: path
 *          name: page
 *          schema:
 *            type: integer
 *          required: true
 *      responses:
 *        200:
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#definitions/200'
 */
