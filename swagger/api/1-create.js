/**
 * @swagger
 * /car_park/v1/slots/lists:
 *   post:
 *      description: Create parking lot by car size
 *      tags:
 *          - Car park
 *      requestBody:
 *          content:
 *              'application/json':
 *                  schema:
 *                      $ref: '#definitions/CreatePark'
 *      responses:
 *        200:
 *          content:
 *              application/json:
 *                  schema:
 *                      $ref: '#definitions/200'
 */
