/**
 * @swagger
 *
 *              definitions:
 *                  CreatePark:
 *                      required:
 *                          - slotFloor
 *                          - total
 *                          - carSize
 *                      properties:
 *                          slotFloor:
 *                              example: f1
 *                              type: string
 *                          total:
 *                              example: 5
 *                              type: integer
 *                          carSize:
 *                              example: 1
 *                              type: integer
 */
