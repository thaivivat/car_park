/**
 * @swagger
 *
 *              definitions:
 *                   ParkCar:
 *                      required:
 *                          - slotID
 *                          - slotPlate
 *                          - slotStatus
 *                      properties:
 *                          slotID:
 *                              example: 200
 *                              type: integer
 *                          slotPlate:
 *                              example: 8743
 *                              type: string
 *                          slotStatus:
 *                              type: integer
 *                              description: 0 for park, 1 for leave
 */
