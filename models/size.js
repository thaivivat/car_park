module.exports = (sequelize, Sequelize) => {
    return sequelize.define(
        'sizes',
        {
            sizeId: {
                type: Sequelize.INTEGER(11),
                field: 'size_id',
                primaryKey: true,
            },
            sizeName: {
                type: Sequelize.STRING,
                field: 'size_name',
            },
        },
        {
            freezeTableName: true, // Model tableName will be the same as the model name
            timestamps: false,
            underscored: false,
        }
    );
};
