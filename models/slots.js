module.exports = (sequelize, Sequelize) => {
    return sequelize.define(
        'slots',
        {
            slotID: {
                type: Sequelize.INTEGER(11),
                field: 'slot_id',
                primaryKey: true,
            },
            slotPlate: {
                type: Sequelize.STRING,
                field: 'slot_plate',
                index: true,
            },
            slotStatus: {
                type: Sequelize.TINYINT,
                field: 'slot_status',
                defaultValue: 1,
                index: true,
            },
            slotFloor: {
                type: Sequelize.STRING,
                field: 'slot_floor',
                index: true,
            },
            slotParkTime: {
                type: Sequelize.DATE,
                field: 'slot_park_time',
            },
            slotParkLeave: {
                type: Sequelize.DATE,
                field: 'slot_park_leave',
            },
            carSize: {
                type: Sequelize.TINYINT,
                field: 'size_id',
                index: true,
            },
        },
        {
            freezeTableName: true, // Model tableName will be the same as the model name
            timestamps: false,
            underscored: false,
        }
    );
};
