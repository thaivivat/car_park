const db = require('../utilities/db/db.mysql');
const Slots = require('../models/slots.js')(db.sequelizeConnect, db.Sequelize);
const Sizes = require('../models/size.js')(db.sequelizeConnect, db.Sequelize);

Slots.belongsTo(Sizes, {
    foreignKey: 'size_id',
});
Sizes.hasOne(Slots, {
    foreignKey: 'size_id',
});

const limit = 10;
class SlotStore {
    async Create(slotFloor, carSize, total) {
        try {
            const slots = [];
            for (let index = 1; index <= total; index++) {
                const slot = {
                    slotFloor: slotFloor,
                    carSize: carSize,
                };
                slots.push(slot);
            }
            await Slots.bulkCreate(slots);
        } catch (e) {
            return null;
        }
    }

    async FindPark(slotID, status) {
        try {
            const condition = {
                where: { slot_id: slotID, slot_status: status },
            };

            const result = await Slots.findOne(condition);

            return !!result;
        } catch (e) {
            return false;
        }
    }

    async Park(slotId, slotPlate) {
        try {
            const condition = { where: { slot_id: slotId } };
            const slot = {
                slotPlate: slotPlate,
                slotStatus: 0, // Park
                slotParkTime: Date.now(),
            };
            await Slots.update(slot, condition);
        } catch (e) {
            return false;
        }
    }

    async Leave(slotId) {
        try {
            const condition = { where: { slot_id: slotId } };
            const slot = {
                slotStatus: 1, // Leave
                slotParkLeave: Date.now(),
            };
            await Slots.update(slot, condition);
        } catch (e) {
            return false;
        }
    }

    async GetSlot(slotID) {
        try {
            const condition = {
                where: { slot_id: slotID },
            };

            return await Slots.findOne(condition);
        } catch (e) {
            return false;
        }
    }

    async GetListsPlate(carSize, page) {
        try {
            const mainQuery = {
                where: { slot_status: 0, size_id: carSize },
                include: [
                    {
                        model: Sizes,
                    },
                ],
                order: db.Sequelize.literal('slots.slot_park_time DESC'),
                offset: (page - 1) * limit,
                limit: limit,
            };

            const listQuery = {
                ...mainQuery,
                offset: (page - 1) * limit,
                limit: limit,
            };

            const lists = {
                lists: await Slots.findAll(listQuery),
                total: await Slots.count(mainQuery),
            };
            return lists;
        } catch (e) {
            return false;
        }
    }

    async GetFloors() {
        try {
            const mainQuery = {
                attributes: [
                    [
                        db.Sequelize.fn(
                            'DISTINCT',
                            db.Sequelize.col('slot_floor')
                        ),
                        'floor',
                    ],
                ],
                order: db.Sequelize.literal('slots.slot_floor ASC'),
                raw: true,
            };
            const floors = await Slots.findAll(mainQuery);
            return floors;
        } catch (e) {
            return false;
        }
    }

    async GetFloorStatus(floor) {
        try {
            const query = `SELECT 
        slot_floor as floor,
        COUNT(slot_id) as total,
        COUNT(CASE WHEN slot_status = 1 then 0 ELSE NULL END) as "Available",
        COUNT(CASE WHEN slot_status = 0 then 1 ELSE NULL END) as "Unavailable"
        FROM slots 
        WHERE slot_floor = '${floor}'`;
            const floors = await db.sequelizeConnect.query(query, {
                type: db.sequelizeConnect.QueryTypes.SELECT,
            });
            return floors[0];
        } catch (e) {
            return false;
        }
    }

    async Allocated(carSize) {
        try {
            const mainQuery = {
                attributes: [
                    [
                        db.Sequelize.fn(
                            'DISTINCT',
                            db.Sequelize.col('slot_floor')
                        ),
                        'floor',
                    ],
                ],
                order: db.Sequelize.literal('slots.slot_floor ASC'),
                raw: true,
                where: { size_id: carSize, slot_status: 1 },
            };
            const floors = await Slots.findAll(mainQuery);
            return floors;
        } catch (e) {
            return false;
        }
    }
}

module.exports = new SlotStore();
