const SlotStore = require('../../repositories/SlotStore');
const { validationResult } = require('express-validator');
class SlotController {
    async createSlots(req, res) {
        const result = { code: 'success', message: 'success' };
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    code: 'invalid_request',
                    message: 'invalid_request',
                    errors: errors.array(),
                });
            }

            await SlotStore.Create(
                req.body.slotFloor,
                req.body.carSize,
                req.body.total
            );

            return res.status(200).json(result);
        } catch (e) {
            return res.status(500).send(e.toString());
        }
    }

    async park(req, res) {
        const result = { code: 'success', message: 'success' };
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    code: 'invalid_request',
                    message: 'invalid_request',
                    errors: errors.array(),
                });
            }

            let checkStatus;
            switch (req.body.slotStatus) {
                case 0: // For Park to car
                    checkStatus = await SlotStore.FindPark(req.body.slotID, 1);
                    if (checkStatus)
                        await SlotStore.Park(
                            req.body.slotID,
                            req.body.slotPlate
                        );
                    break;

                default:
                    checkStatus = await SlotStore.FindPark(req.body.slotID, 0);
                    if (checkStatus) await SlotStore.Leave(req.body.slotID);
                    break;
            }

            if (!checkStatus) {
                return res.status(400).json({
                    code: 'status_invalid',
                    message: 'status_invalid',
                });
            }

            return res.status(200).json(result);
        } catch (e) {
            return res.status(500).send(e.toString());
        }
    }

    async get(req, res) {
        const result = { code: 'success', message: 'success' };
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    code: 'invalid_request',
                    message: 'invalid_request',
                    errors: errors.array(),
                });
            }

            result.data = await SlotStore.GetSlot(req.params.slotID);

            return res.status(200).json(result);
        } catch (e) {
            return res.status(500).send(e.toString());
        }
    }

    async getListsPlateNumber(req, res) {
        const result = { code: 'success', message: 'success' };
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    code: 'invalid_request',
                    message: 'invalid_request',
                    errors: errors.array(),
                });
            }

            const lists = await SlotStore.GetListsPlate(
                req.params.carSize,
                req.params.page
            );

            result.data = {
                lists: lists.lists,
                total: lists.total,
            };

            return res.status(200).json(result);
        } catch (e) {
            return res.status(500).send(e.toString());
        }
    }

    async getLists(_, res) {
        const result = { code: 'success', message: 'success' };
        try {
            const floorsName = await SlotStore.GetFloors();
            const floors = [];
            for (const kfloor in floorsName) {
                if (Object.hasOwnProperty.call(floorsName, kfloor)) {
                    const floor = floorsName[kfloor].floor;
                    const floorStatus = await SlotStore.GetFloorStatus(floor);
                    floors.push(floorStatus);
                }
            }

            result.data = floors;

            return res.status(200).json(result);
        } catch (e) {
            return res.status(500).send(e.toString());
        }
    }

    async allocated(req, res) {
        const result = { code: 'success', message: 'success' };
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({
                    code: 'invalid_request',
                    message: 'invalid_request',
                    errors: errors.array(),
                });
            }
            result.data = await SlotStore.Allocated(req.params.carSize);
            return res.status(200).json(result);
        } catch (e) {
            return res.status(500).send(e.toString());
        }
    }
}

module.exports = new SlotController();
