const { expect, app, request } = require('../../common');

describe('GET /car_park/v1/slots/lists', () => {
    it('[4] Get Status of parking lot', (done) => {
        request(app)
            .get('/car_park/v1/slots/lists')
            .send()
            .then((res) => {
                const body = res.body;
                expect(res.statusCode).equal(200);
                expect(body).to.contain.property('code');
                expect(body).to.contain.property('message');
                done();
            })
            .catch((err) => done(err));
    });
});
