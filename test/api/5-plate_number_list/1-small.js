const { expect, app, request } = require('../../common');

describe('GET /car_park/v1/slots/lists/plate/1/1', () => {
    it('[5] Get plate number by car size small', (done) => {
        request(app)
            .get('/car_park/v1/slots/lists/plate/1/1')
            .send()
            .then((res) => {
                const body = res.body;
                expect(res.statusCode).equal(200);
                expect(body).to.contain.property('code');
                expect(body).to.contain.property('message');
                expect(body).to.contain.property('data');
                done();
            })
            .catch((err) => done(err));
    });
});
