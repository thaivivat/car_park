const { expect, app, request } = require('../../common');

describe('POST /car_park/v1/slots/lists', () => {
    it('[1] Create parking lot by car size large', (done) => {
        request(app)
            .post('/car_park/v1/slots/lists')
            .send({
                slotFloor: 'f1',
                total: 10,
                carSize: 3,
            })
            .then((res) => {
                const body = res.body;
                expect(res.statusCode).equal(200);
                expect(body).to.contain.property('code');
                expect(body).to.contain.property('message');
                done();
            })
            .catch((err) => done(err));
    });
});
