const { expect, app, request } = require('../../common');

describe('PATCH /car_park/v1/slots/park', () => {
    it('[3] Leave the car', (done) => {
        request(app)
            .patch('/car_park/v1/slots/park')
            .send({
                slotID: 6,
                slotStatus: 1,
            })
            .then((res) => {
                const body = res.body;
                expect(body).to.contain.property('code');
                expect(body).to.contain.property('message');
                done();
            })
            .catch((err) => done(err));
    });
});
