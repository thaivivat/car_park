const { expect, app, request } = require('../../common');

describe('GET /v1/slots/allocated/2', () => {
    it('[6] Allocate parking by car size medium', (done) => {
        request(app)
            .get('/car_park/v1/slots/allocated/2')
            .send()
            .then((res) => {
                const body = res.body;
                expect(res.statusCode).equal(200);
                expect(body).to.contain.property('code');
                expect(body).to.contain.property('message');
                expect(body).to.contain.property('data');
                done();
            })
            .catch((err) => done(err));
    });
});
