const Sequelize = require('sequelize');

const databaseName = process.env.DATABASE_NAME;
const databaseUsername = process.env.DATABASE_USERNAME;
const databasePassword = process.env.DATABASE_PASSWORD;
const databaseHost = process.env.DATABASE_HOST;

const sequenlizeDialect = process.env.SEQUENLIZE_DIALECT || 'mysql';
const sequenlizeMax = parseInt(process.env.SEQUENLIZE_POOL_MAX) || 5;
const sequenlizeMin = parseInt(process.env.SEQUENLIZE_POOL_MIN) || 0;
const sequenlizeAcquire = process.env.SEQUENLIZE_POOL_ACQUIRE || '30000';
const sequenlizeIdle = process.env.SEQUENLIZE_POOL_IDLE || '10000';

const sequelizeConnect = new Sequelize(
    databaseName,
    databaseUsername,
    databasePassword,
    {
        host: databaseHost,
        dialect: sequenlizeDialect,
        pool: {
            max: sequenlizeMax,
            min: sequenlizeMin,
            acquire: sequenlizeAcquire,
            idle: sequenlizeIdle,
        },
    }
);

const db = {};
db.Sequelize = Sequelize;
db.sequelizeConnect = sequelizeConnect;

module.exports = db;
