const cors = require('cors');
const createError = require('http-errors');
const express = require('express');
const app = express();
app.use(cors());

// ---- Start API Doc
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const options = {
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'Car park API',
            version: '1.0.0',
            description: 'Car park Service',
        },
    },
    apis: ['./swagger/*/*.js', './swagger/models/*.js'],
    security: [{ auth: [] }],
    basePath: '/',
};
const swaggerSpec = swaggerJSDoc(options);
app.use('/api-doc', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
// ---- End API Doc

require('./routes/index.js')(app);

app.use(function (req, res, next) {
    next(
        createError(
            404,
            'The requested endpoint with the method given was not found'
        )
    );
});

module.exports = app;
